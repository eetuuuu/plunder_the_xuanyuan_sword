// ActDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Editor.h"
#include "ActDlg.h"
#include "Scene.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CActDlg dialog


CActDlg::CActDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CActDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CActDlg)
	m_nIndex = -1;
	m_nPara1 = 0;
	m_nPara2 = 0;
	m_nPara3 = 0;
	m_nPara4 = 0;
	m_strPara1 = _T("");
	m_strPara2 = _T("");
	m_strPara3 = _T("");
	m_strPara4 = _T("");
	m_nPara5 = 0;
	m_nPara6 = 0;
	m_strPara5 = _T("");
	m_strPara6 = _T("");
	//}}AFX_DATA_INIT
}


void CActDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CActDlg)
	DDX_Control(pDX, IDC_EDIT6, m_editPara6);
	DDX_Control(pDX, IDC_EDIT5, m_editPara5);
	DDX_Control(pDX, IDC_EDIT4, m_editPara4);
	DDX_Control(pDX, IDC_EDIT3, m_editPara3);
	DDX_Control(pDX, IDC_EDIT2, m_editPara2);
	DDX_Control(pDX, IDC_EDIT1, m_editPara1);
	DDX_Control(pDX, IDC_COMBO, m_comboCommand);
	DDX_CBIndex(pDX, IDC_COMBO, m_nIndex);
	DDX_Text(pDX, IDC_EDIT1, m_nPara1);
	DDX_Text(pDX, IDC_EDIT2, m_nPara2);
	DDX_Text(pDX, IDC_EDIT3, m_nPara3);
	DDX_Text(pDX, IDC_EDIT4, m_nPara4);
	DDX_Text(pDX, IDC_TEXT1, m_strPara1);
	DDX_Text(pDX, IDC_TEXT2, m_strPara2);
	DDX_Text(pDX, IDC_TEXT3, m_strPara3);
	DDX_Text(pDX, IDC_TEXT4, m_strPara4);
	DDX_Text(pDX, IDC_EDIT5, m_nPara5);
	DDX_Text(pDX, IDC_EDIT6, m_nPara6);
	DDX_Text(pDX, IDC_TEXT5, m_strPara5);
	DDX_Text(pDX, IDC_TEXT6, m_strPara6);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CActDlg, CDialog)
	//{{AFX_MSG_MAP(CActDlg)
	ON_CBN_SELCHANGE(IDC_COMBO, OnSelchangeCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CActDlg message handlers

int CActDlg::DoModal(int nType,ACT* pAct)
{
	// TODO: Add your specialized code here and/or call the base class
	m_nType  = nType;
	m_pAct   = pAct;
	m_nIndex = m_pAct->nActType-1;
	m_nPara1 = m_pAct->nPara1;
	m_nPara2 = m_pAct->nPara2;
	m_nPara3 = m_pAct->nPara3;
	m_nPara4 = m_pAct->nPara4;
	m_nPara5 = m_pAct->nPara5;
	m_nPara6 = m_pAct->nPara6;
	//
	return CDialog::DoModal();
}

void CActDlg::UpdatePrompt()
{
	UpdateData(TRUE);
	//
	switch( m_nIndex )
	{
	case 0:
		m_strPara1 = "图片文件编号";
		m_strPara2 = "";
		m_strPara3 = "";
		m_strPara4 = "";
		m_strPara5 = "";
		m_strPara6 = "";
		m_editPara1.ShowWindow(SW_SHOW);
		m_editPara2.ShowWindow(SW_HIDE);
		m_editPara3.ShowWindow(SW_HIDE);
		m_editPara4.ShowWindow(SW_HIDE);
		m_editPara5.ShowWindow(SW_HIDE);
		m_editPara6.ShowWindow(SW_HIDE);
		break;
	case 1:
		m_strPara1 = "MIDI文件编号";
		m_strPara2 = "";
		m_strPara3 = "";
		m_strPara4 = "";
		m_strPara5 = "";
		m_strPara6 = "";
		m_editPara1.ShowWindow(SW_SHOW);
		m_editPara2.ShowWindow(SW_HIDE);
		m_editPara3.ShowWindow(SW_HIDE);
		m_editPara4.ShowWindow(SW_HIDE);
		m_editPara5.ShowWindow(SW_HIDE);
		m_editPara6.ShowWindow(SW_HIDE);
		break;
	case 2:
		m_strPara1 = "";
		m_strPara2 = "";
		m_strPara3 = "";
		m_strPara4 = "";
		m_strPara5 = "";
		m_strPara6 = "";
		m_editPara1.ShowWindow(SW_HIDE);
		m_editPara2.ShowWindow(SW_HIDE);
		m_editPara3.ShowWindow(SW_HIDE);
		m_editPara4.ShowWindow(SW_HIDE);
		m_editPara5.ShowWindow(SW_HIDE);
		m_editPara6.ShowWindow(SW_HIDE);
		break;
	case 3:
		m_strPara1 = "WAV文件编号";
		m_strPara2 = "";
		m_strPara3 = "";
		m_strPara4 = "";
		m_strPara5 = "";
		m_strPara6 = "";
		m_editPara1.ShowWindow(SW_SHOW);
		m_editPara2.ShowWindow(SW_HIDE);
		m_editPara3.ShowWindow(SW_HIDE);
		m_editPara4.ShowWindow(SW_HIDE);
		m_editPara5.ShowWindow(SW_HIDE);
		m_editPara6.ShowWindow(SW_HIDE);
		break;
	case 4:
		if( m_nType == 1 )
		{
			//对话场
			m_strPara1 = "屏幕上所显示图片的编号（1到10）";
			m_strPara2 = "图片文件编号";
			m_strPara3 = "显示位置的X坐标";
			m_strPara4 = "显示位置的Y坐标";
			m_strPara5 = "图片长度";
			m_strPara6 = "图片宽度";
			m_editPara6.ShowWindow(SW_SHOW);
		}
		else if( m_nType == 2 )
		{
			//战斗场
			m_strPara1 = "敌人编号（0到39）";
			m_strPara2 = "战斗模式的编号（1到9）";
			m_strPara3 = "X坐标";
			m_strPara4 = "Y坐标";
			m_strPara5 = "该敌人首次出现的周期数（60毫秒一周期）";
			m_strPara6 = "";
			m_editPara6.ShowWindow(SW_HIDE);
		}
		m_editPara1.ShowWindow(SW_SHOW);
		m_editPara2.ShowWindow(SW_SHOW);
		m_editPara3.ShowWindow(SW_SHOW);
		m_editPara4.ShowWindow(SW_SHOW);
		m_editPara5.ShowWindow(SW_SHOW);
		break;
	case 5:
		m_strPara1 = "屏幕上所显示图片的编号（0到9）";
		m_strPara2 = "";
		m_strPara3 = "";
		m_strPara4 = "";
		m_strPara5 = "";
		m_strPara6 = "";
		m_editPara1.ShowWindow(SW_SHOW);
		m_editPara2.ShowWindow(SW_HIDE);
		m_editPara3.ShowWindow(SW_HIDE);
		m_editPara4.ShowWindow(SW_HIDE);
		m_editPara5.ShowWindow(SW_HIDE);
		m_editPara6.ShowWindow(SW_HIDE);
		break;
	case 6:
		m_strPara1 = "对话文本所在的文件编号";
		m_strPara2 = "对话文本所在的起始行数";
		m_strPara3 = "选项的个数（取值1到4，0和1表示无选项）";
		m_strPara4 = "";
		m_strPara5 = "";
		m_strPara6 = "";
		m_editPara1.ShowWindow(SW_SHOW);
		m_editPara2.ShowWindow(SW_SHOW);
		m_editPara3.ShowWindow(SW_SHOW);
		m_editPara4.ShowWindow(SW_HIDE);
		m_editPara5.ShowWindow(SW_HIDE);
		m_editPara6.ShowWindow(SW_HIDE);
		break;
	case 7:
		m_strPara1 = "需要延时的毫秒数";
		m_strPara2 = "";
		m_strPara3 = "";
		m_strPara4 = "";
		m_strPara5 = "";
		m_strPara6 = "";
		m_editPara1.ShowWindow(SW_SHOW);
		m_editPara2.ShowWindow(SW_HIDE);
		m_editPara3.ShowWindow(SW_HIDE);
		m_editPara4.ShowWindow(SW_HIDE);
		m_editPara5.ShowWindow(SW_HIDE);
		m_editPara6.ShowWindow(SW_HIDE);
		break;
	}
	//
	UpdateData(FALSE);
}

BOOL CActDlg::CheckValue()
{
	UpdateData(TRUE);
	//
	switch( m_nIndex )
	{
	case 0:
		if( m_nPara1<0 )
		{
			MessageBox("参数1，图片文件编号必须是正数！",NULL,MB_OK);
			return FALSE;
		}
		break;
	case 1:
		if( m_nPara1<0 )
		{
			MessageBox("参数1，MIDI文件编号必须是正数！",NULL,MB_OK);
			return FALSE;
		}
		break;
	case 2:
		break;
	case 3:
		if( m_nPara1<0 )
		{
			MessageBox("参数1，WAV文件编号必须是正数！",NULL,MB_OK);
			return FALSE;
		}
		break;
	case 4:
		if( m_nType == 1 )
		{
			//对话场
			if( m_nPara1<0 || m_nPara1>9 )
			{
				MessageBox("参数1屏幕图片编号的取值范围是（0到9）！",NULL,MB_OK);
				return FALSE;
			}
			if( m_nPara2<0 )
			{
				MessageBox("参数2，图片文件编号必须是正数！",NULL,MB_OK);
				return FALSE;
			}
			if( m_nPara3<0 || m_nPara3>639 )
			{
				MessageBox("参数3，图片左上角的X坐标的取值范围是（0到639）！",NULL,MB_OK);
				return FALSE;
			}
			if( m_nPara4<0 || m_nPara4>479 )
			{
				MessageBox("参数4，图片左上角的Y坐标的取值范围是（0到479）！",NULL,MB_OK);
				return FALSE;
			}
			if( m_nPara5<0 )
			{
				MessageBox("参数5，图片的宽度必须是正数！",NULL,MB_OK);
				return FALSE;
			}
			if( m_nPara6<0 )
			{
				MessageBox("参数6，图片的高度必须是正数！",NULL,MB_OK);
				return FALSE;
			}
		}
		else if( m_nType == 2 )
		{
			//战斗场
			if( m_nPara1<0 || m_nPara1>39 )
			{
				MessageBox("参数1敌人编号的取值范围是（0到39）！",NULL,MB_OK);
				return FALSE;
			}
			if( m_nPara2<1 || m_nPara2>9 )
			{
				MessageBox("参数2战斗模式的取值范围是（1到9）！",NULL,MB_OK);
				return FALSE;
			}
			if( m_nPara3<0 || m_nPara3>639 )
			{
				MessageBox("参数3，出现位置左上角的X坐标的取值范围是（0到639）！",NULL,MB_OK);
				return FALSE;
			}
			if( m_nPara4<0 || m_nPara4>479 )
			{
				MessageBox("参数4，出现位置左上角的Y坐标的取值范围是（0到479）！",NULL,MB_OK);
				return FALSE;
			}
			if( m_nPara5<1 )
			{
				MessageBox("参数5，该敌人首次出现的延时周期必须大于等于1！",NULL,MB_OK);
				return FALSE;
			}
		}
		break;
	case 5:
		if( m_nPara1<0 || m_nPara1>9 )
		{
			MessageBox("参数1屏幕图片编号的取值范围是（0到9）！",NULL,MB_OK);
			return FALSE;
		}
		break;
	case 6:
		if( m_nPara1<0 )
		{
			MessageBox("参数1，对话文件的编号必须是正数！",NULL,MB_OK);
			return FALSE;
		}
		if( m_nPara2<1 )
		{
			MessageBox("参数2，对话文本所在的行数要从1开始计数！",NULL,MB_OK);
			return FALSE;
		}
		if( m_nPara3<0 || m_nPara3>4 )
		{
			MessageBox("参数3，对话框最多4个选项！",NULL,MB_OK);
			return FALSE;
		}
		break;
	case 7:
		if( m_nPara1<1 )
		{
			MessageBox("延时时间必须大于等于1毫秒！",NULL,MB_OK);
			return FALSE;
		}
		break;
	}
	//
	return TRUE;
}

BOOL CActDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	// TODO: Add extra initialization here
	//根据场类型的不同，给Combo设置命令项
	m_comboCommand.AddString("01、设置背景（SETBK）");
	m_comboCommand.AddString("02、播放MIDI文件（PLAYMIDI）");
	m_comboCommand.AddString("03、停止MIDI播放（STOPMIDI）");
	m_comboCommand.AddString("04、播放WAV文件（PLAYWAV）");
	if( m_nType == 1 )
	{
		m_comboCommand.AddString("05、显示图片（APPEAR）");
		m_comboCommand.AddString("06、图片消失（DISAPP）");
		m_comboCommand.AddString("07、显示对话（DLALOG）");
		m_comboCommand.AddString("08、延时（DELAY）");
	}
	else if( m_nType == 2 )
	{
		if( m_nIndex == 8 )
			m_nIndex = 4;
		m_comboCommand.AddString("05、配置敌人（ENEMY）");
	}
	m_comboCommand.SetCurSel( m_nIndex );
	//
	UpdatePrompt();
	//
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CActDlg::OnSelchangeCombo() 
{
	// TODO: Add your control notification handler code here
	UpdatePrompt();
}

void CActDlg::OnOK() 
{
	// TODO: Add extra validation here
	if( CheckValue() )
	{
		//保存数据
		if( m_nType==2 && m_nIndex==4 )
		{
			m_nIndex = 8;
		}
		memset( m_pAct, 0, sizeof(ACT) );
		m_pAct->nActType = (m_nIndex+1);
		switch( m_nIndex+1 )
		{
		case 3:
			break;
		case 1:
		case 2:
		case 4:
		case 6:
		case 8:
			m_pAct->nPara1   = m_nPara1;
			break;
		case 7:
			m_pAct->nPara1   = m_nPara1;
			m_pAct->nPara2   = m_nPara2;
			m_pAct->nPara3   = m_nPara3;
			break;
		case 9:
			m_pAct->nPara1   = m_nPara1;
			m_pAct->nPara2   = m_nPara2;
			m_pAct->nPara3   = m_nPara3;
			m_pAct->nPara4   = m_nPara4;
			m_pAct->nPara5   = m_nPara5;
			break;
		case 5:
			m_pAct->nPara1   = m_nPara1;
			m_pAct->nPara2   = m_nPara2;
			m_pAct->nPara3   = m_nPara3;
			m_pAct->nPara4   = m_nPara4;
			m_pAct->nPara5   = m_nPara5;
			m_pAct->nPara6   = m_nPara6;
			break;
		}
	}
	else
	{
		return;
	}
	CDialog::OnOK();
}
