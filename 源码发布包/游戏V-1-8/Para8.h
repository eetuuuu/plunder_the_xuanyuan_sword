void CMainFrame::Para8Init(void)
{
/////////////////////////////////////////////////////////////////////////////
//调入所需对话，主角图片，地图文件群。
	LoadDialog("dlg\\para8.dlg");//调入对话
	LoadActor("pic\\hero.bmp");//调入主角
	deleteallbmpobjects();
/////////////////////////////////////////////////////////////////////////////
//调入所需头像
	hbmp_Photo0=(HBITMAP)LoadImage(NULL,"pic\\pic300.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//司徒
	hbmp_Photo1=(HBITMAP)LoadImage(NULL,"pic\\pic302.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//沙子
	hbmp_Photo2=(HBITMAP)LoadImage(NULL,"pic\\pic301.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//莫隆
	hbmp_Photo3=(HBITMAP)LoadImage(NULL,"pic\\pic320.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//德里安
	hbmp_Photo4=(HBITMAP)LoadImage(NULL,"pic\\pic315.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜（1）
	hbmp_Photo5=(HBITMAP)LoadImage(NULL,"pic\\pic316.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜（2）
	hbmp_Photo6=(HBITMAP)LoadImage(NULL,"pic\\pic310.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（1）
	hbmp_Photo7=(HBITMAP)LoadImage(NULL,"pic\\pic311.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺（2）
	hbmp_Photo8=(HBITMAP)LoadImage(NULL,"pic\\pic325.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//阿萨德
	::DeleteObject(hbmp_Photo9);
/////////////////////////////////////////////////////////////////////////////
//调入其他图片
	hbmp_0=(HBITMAP)LoadImage(NULL,"pic\\hero.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺
	hbmp_1=(HBITMAP)LoadImage(NULL,"pic\\pic107.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//荻娜
	hbmp_2=(HBITMAP)LoadImage(NULL,"pic\\pic108.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//老头
	hbmp_3=(HBITMAP)LoadImage(NULL,"pic\\pic100.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//司徒
	hbmp_4=(HBITMAP)LoadImage(NULL,"pic\\pic116.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//阿萨德
	hbmp_5=(HBITMAP)LoadImage(NULL,"pic\\pic102.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//CIA分子
	hbmp_6=(HBITMAP)LoadImage(NULL,"pic\\pic117.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//恐怖分子
	hbmp_7=(HBITMAP)LoadImage(NULL,"pic\\pic101.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//莫隆
	hbmp_8=(HBITMAP)LoadImage(NULL,"pic\\pic110.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//白莲教组图
	hbmp_9=(HBITMAP)LoadImage(NULL,"pic\\pic021.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//军舰
	hbmp_10=(HBITMAP)LoadImage(NULL,"pic\\pic210.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//汗叹倒
	hbmp_11=(HBITMAP)LoadImage(NULL,"pic\\pic007.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//爆炸
	hbmp_12=(HBITMAP)LoadImage(NULL,"system\\enemies\\ENEMY06.BMP",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//怪兽
	hbmp_13=(HBITMAP)LoadImage(NULL,"pic\\pic106.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//卡诺拔剑特图
	::DeleteObject(hbmp_14);
	hbmp_15=(HBITMAP)LoadImage(NULL,"pic\\pic114.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//轩辕剑
	hbmp_16=(HBITMAP)LoadImage(NULL,"pic\\pic017.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//火山
	hbmp_17=(HBITMAP)LoadImage(NULL,"pic\\pic015.bmp",IMAGE_BITMAP,0,0,LR_LOADFROMFILE);//岛屿
/////////////////////////////////////////////////////////////////////////////
	OpenMap("map\\maps110.bmp","map\\map110a.map","map\\map110a.npc");
/////////////////////////////////////////////////////////////////////////////
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para8Info(int type)
{
	int i;

if(st_sub1==0){
	if(st_dialog==0){
		m_info_st=9;
		m_oldinfo_st=9;
		current.x=145;
		current.y=85;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_6);//恐怖分子
		resetblt();
		blt(128,192,32,32,0,96,0,0,&BuffDC);
		resetblt();
		blt(160,192,32,32,32,96,32,0,&BuffDC);
		resetblt();
		blt(128,224,32,32,0,128,0,32,&BuffDC);
		resetblt();
		blt(128,256,32,32,0,160,0,64,&BuffDC);
		resetblt();
		blt(160,256,32,32,32,160,32,64,&BuffDC);
		MemDC.SelectObject(hbmp_4);//阿萨德
		resetblt();
		blt(192,224,64,32,64,128,0,128,&BuffDC);
		MemDC.SelectObject(hbmp_1);//荻娜
		resetblt();
		blt(512,208,32,48,96,48,0,48,&BuffDC);
		MemDC.SelectObject(hbmp_12);//怪兽
		resetblt();
		blt(192,96,32,32,0,96,0,0,&BuffDC);
		resetblt();
		blt(320,160,32,32,32,96,32,0,&BuffDC);
		resetblt();
		blt(384,256,32,32,0,128,0,32,&BuffDC);
		resetblt();
		blt(128,416,32,32,0,160,0,64,&BuffDC);
		resetblt();
		blt(160,448,32,32,32,160,32,64,&BuffDC);
		resetblt();
		blt(352,128,32,32,0,96,0,0,&BuffDC);
		resetblt();
		blt(128,128,32,32,32,96,32,0,&BuffDC);
		resetblt();
		blt(320,352,32,32,0,128,0,32,&BuffDC);
		resetblt();
		blt(288,384,32,32,0,160,0,64,&BuffDC);
		resetblt();
		blt(64,224,32,32,32,160,32,64,&BuffDC);
		PlayMidi("midi\\p1015.mid");
		vopen();
		Dialog(2,0,0,0,1);
		st_dialog=1;
	}
	else if(st_dialog==1){
		Dialog(4,41,0,0,2);
		st_dialog=10;
	}
	else if(st_dialog==10){
		vclose();
		m_info_map=1;//打开地图支持
		m_oldinfo_map=1;///////////
		base_x=190;//设定地图基准点
		base_y=89;///////////////
		current.x=180;
		current.y=26;
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		azimuth= AZIMUTH_LEFT;
		movest=0;
		npc[current.x][current.y]=800;//将主角绘制在npc矩阵上
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		DrawActor(1);
		timelimit=600;//设定时间限制
		BuffDC.BitBlt(50,45,84,22,&InfoDC,451,108,SRCAND);
		BuffDC.BitBlt(50,45,84,22,&InfoDC,368,108,SRCINVERT);
		BuffDC.StretchBlt(150,50,timelimit,9,&InfoDC,751,87,1,9,SRCCOPY);//时间
		vopen();
		Dialog(2,0,0,0,3);
		st_dialog=11;
	}
	else if(st_dialog==11){
		Dialog(2,0,0,0,4);
		st_dialog=12;
	}
	else if(st_dialog==12){
		m_info_st=100;
		m_oldinfo_st=100;
		SetActTimer();
		blood=200;//满血
		bullet=65;//限制子弹
		super_bullet=TRUE;
		st_dialog=2000;
	}
}//st_sub1=0的部分结束
else if(st_sub1==1){
	if(st_dialog==0){
		vclose();
		OpenMap("map\\maps110.bmp","map\\map110a.map","map\\map110a.npc");
		current.x=150;
		current.y=75;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_6);//恐怖分子
		resetblt();
		blt(128,192,32,32,64,128,64,32,&BuffDC);
		resetblt();
		blt(160,192,32,32,64,128,64,32,&BuffDC);
		resetblt();
		blt(128,224,32,32,64,128,64,32,&BuffDC);
		resetblt();
		blt(128,256,32,32,64,128,64,32,&BuffDC);
		resetblt();
		blt(160,256,32,32,64,128,64,32,&BuffDC);
		MemDC.SelectObject(hbmp_4);//阿萨德
		resetblt();
		blt(192,224,64,32,64,128,0,128,&BuffDC);
		MemDC.SelectObject(hbmp_1);//荻娜
		resetblt();
		blt(512,208,32,48,96,48,0,48,&BuffDC);
		MemDC.SelectObject(hbmp_13);//卡诺
		resetblt();
		blt(452,208,64,48,0,48,0,0,&BuffDC);
		PlayMidi("midi\\p1011.mid");
		vopen();
		Dialog(4,0,31,0,10);
		st_dialog=2;
	}
	else if(st_dialog==2){//卡诺拔出轩辕剑
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_13);//卡诺
		PlayWave("snd\\open.wav");
		blt(452,208,64,48,0,48,0,0,pdc);
		GameSleep(200);
		blt(452,208,64,48,64,48,64,0,pdc);
		GameSleep(200);
		blt(452,208,64,48,128,48,128,0,pdc);
		GameSleep(200);
		blt(452,208,64,48,192,48,192,0,pdc);
		GameSleep(200);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,41,0,0,11);
		st_dialog=3;
	}
	else if(st_dialog==3){
		Dialog(4,0,31,0,12);
		st_dialog=5;
	}
	else if(st_dialog==5){//沙子到
		CDC *pdc=GetDC();
		resetblt();
		MemDC.SelectObject(hbmp_8);//白莲教组图
		for(i=0;i<8;i++)
		{
			blt(272,(488-(i*16)),96,64,192,64,0,64,pdc);
			GameSleep(100);
			blt(272,(480-(i*16)),96,64,288,64,96,64,pdc);
			GameSleep(100);
		}
		MemDC.SelectObject(hbmp_3);//司徒
		resetblt();
		for(i=0;i<8;i++)
		{
			blt(224,(488-(i*16)),32,32,64,128,0,128,pdc);
			GameSleep(100);
			blt(224,(480-(i*16)),32,32,96,128,32,128,pdc);
			GameSleep(100);
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(4,6,0,0,13);
		st_dialog=6;
	}
	else if(st_dialog==6){
		Dialog(4,6,0,0,14);
		st_dialog=7;
	}
	else if(st_dialog==7){
		Dialog(4,6,0,0,15);
		st_dialog=10;
	}
	else if(st_dialog==10){//机枪扫射恐怖分子
		lclose();
		OpenMap("map\\maps110.bmp","map\\map110a.map","map\\map110a.npc");
		current.x=147;
		current.y=75;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_6);//恐怖分子
		resetblt();
		blt(528,192,32,32,0,128,0,32,&BuffDC);
		resetblt();
		blt(560,192,32,32,0,128,0,32,&BuffDC);
		resetblt();
		blt(528,224,32,32,0,128,0,32,&BuffDC);
		resetblt();
		blt(528,256,32,32,0,128,0,32,&BuffDC);
		resetblt();
		blt(560,256,32,32,0,128,0,32,&BuffDC);
		MemDC.SelectObject(hbmp_4);//阿萨德
		resetblt();
		blt(560,224,32,32,64,32,0,32,&BuffDC);
		MemDC.SelectObject(hbmp_8);//白莲教组图
		resetblt();
		blt(256,160,64,128,320,160,128,160,&BuffDC);
		MemDC.SelectObject(hbmp_3);//司徒
		resetblt();
		blt(256,288,64,32,64,192,0,192,&BuffDC);
		lopen();
		Dialog(4,6,0,0,16);
		st_dialog=11;
	}
	else if(st_dialog==11){//扫射，然后转到海边。
		CDC *pdc=GetDC();
		for(i=0;i<20;i++){
			PlayWave("snd\\shoot.wav");
			MemDC.SelectObject(hbmp_8);//白莲教组图
			oldx=64;oldy=128;oldlx=256;oldly=160;
			blt(256,160,64,128,320,160,128,160,pdc);
			MemDC.SelectObject(hbmp_3);//司徒
			oldx=64;oldy=32;oldlx=256;oldly=288;
			blt(256,288,64,32,64,192,0,192,pdc);
			GameSleep(50);
			MemDC.SelectObject(hbmp_8);//白莲教组图
			oldx=64;oldy=128;oldlx=256;oldly=160;
			blt(256,160,64,128,320,288,128,288,pdc);
			MemDC.SelectObject(hbmp_3);//司徒
			oldx=64;oldy=32;oldlx=256;oldly=288;
			blt(256,288,64,32,64,224,0,224,pdc);
			GameSleep(50);
		}
		MemDC.SelectObject(hbmp_11);//爆炸图片
		PlayWave("snd\\bomb2.wav");
		for(i=0;i<10;i++){
			pdc->BitBlt(528,192,200,200,&BuffDC,528,192,SRCCOPY);
			pdc->StretchBlt(528,192,108,100,&MemDC,(i*54),50,54,50,SRCAND);
			pdc->StretchBlt(528,192,108,100,&MemDC,(i*54),0,54,50,SRCINVERT);
			GameSleep(100);
		}
		BuffDC.BitBlt(528,192,200,200,&MapDC,528,192,SRCCOPY);
		pdc->BitBlt(528,192,200,200,&BuffDC,528,192,SRCCOPY);
		ReleaseDC(pdc);
		pdc = NULL;
		vclose();//下面转入海边军舰
		m_info_st=9;
		m_oldinfo_st=9;
		OpenMap("map\\maps105.bmp","map\\map105c.map","map\\map105b.npc");
		map[158][168]=786;
		map[162][168]=786;
		current.x=160;
		current.y=172;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_9);//军舰
		resetblt();
		blt(200,300,600,180,0,180,0,0,&BuffDC);
		MemDC.SelectObject(hbmp_6);//恐怖分子
		resetblt();
		blt(320,384,32,32,32,96,32,0,&BuffDC);
		resetblt();
		blt(352,384,32,32,32,96,32,0,&BuffDC);
		resetblt();
		blt(384,384,32,32,32,96,32,0,&BuffDC);
		resetblt();
		blt(416,384,32,32,32,96,32,0,&BuffDC);
		resetblt();
		blt(448,384,32,32,32,96,32,0,&BuffDC);
		resetblt();
		blt(480,384,32,32,32,96,32,0,&BuffDC);
		vopen();
		Dialog(2,0,0,0,17);
		st_dialog=20;
	}
	else if(st_dialog==20){
		Dialog(4,13,0,0,18);
		st_dialog=21;
	}
	else if(st_dialog==21){
		Dialog(4,13,0,0,19);
		st_dialog=22;
	}
	else if(st_dialog==22){
		Dialog(4,13,0,0,20);
		st_dialog=23;
	}
	else if(st_dialog==23){
		Dialog(2,0,0,0,21);
		st_dialog=24;
	}
	else if(st_dialog==24){
		Dialog(4,13,0,0,22);
		st_dialog=25;
	}
	else if(st_dialog==25){
		Dialog(2,0,0,0,23);
		st_dialog=30;
	}
	else if(st_dialog==30){//军舰爆炸
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_11);//爆炸图片
		int a=150;int b=330;
		for(int j=0;j<6;j++){
		PlayWave("snd\\bomb2.wav");
			for(i=0;i<10;i++){
				pdc->BitBlt(a,b,200,200,&BuffDC,a,b,SRCCOPY);
				pdc->StretchBlt(a,b,162,150,&MemDC,(i*54),50,54,50,SRCAND);
				pdc->StretchBlt(a,b,162,150,&MemDC,(i*54),0,54,50,SRCINVERT);
				GameSleep(40);
			}
			BuffDC.BitBlt(a,(b-100),200,300,&MapDC,a,(b-100),SRCCOPY);
			pdc->BitBlt(a,(b-100),200,300,&BuffDC,a,(b-100),SRCCOPY);
			a+=100;
		}
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,53);
		st_dialog=31;
	}
	else if(st_dialog==31){
		PlayMidi("midi\\p1005.mid");
		//切换地图
		oldcurrent.x=current.x;
		oldcurrent.y=current.y;
		strcpy(oldfname01,fname01);
		strcpy(oldfname02,fname02);
		strcpy(oldfname03,fname03);//保存当前的地图
		OpenMap("map\\maps103.bmp","map\\map105a.map","map\\map105a.npc");//调入地图
		current.x=138;
		current.y=40;
		movest=0;
		DrawMap();//准备地图
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		current.x=oldcurrent.x;
		current.y=oldcurrent.y;
		strcpy(fname01,oldfname01);
		strcpy(fname02,oldfname02);
		strcpy(fname03,oldfname03);//恢复地图
		OpenMap(fname01,fname02,fname03);//调入以前的地图
		//
		MemDC.SelectObject(hbmp_3);//司徒
		resetblt();
		blt(544,250,32,32,64,96,0,96,&BuffDC);
		MemDC.SelectObject(hbmp_8);//白莲教群体
		resetblt();
		blt(224,218,64,96,192,224,0,224,&BuffDC);
		MemDC.SelectObject(hbmp_7);//莫隆
		resetblt();
		blt(384,320,32,32,64,64,0,64,&BuffDC);
		MemDC.SelectObject(hbmp_5);//CIA分子
		resetblt();
		blt(352,320,32,32,32,96,32,0,&BuffDC);
		resetblt();
		blt(416,320,32,32,32,96,32,0,&BuffDC);
		MemDC.SelectObject(hbmp_15);//轩辕剑
		resetblt();
		blt(384,200,32,48,96,0,0,0,&BuffDC);
		vopen();
		Dialog(4,6,0,0,24);
		st_dialog=32;
	}
	else if(st_dialog==32){
		Dialog(4,0,1,0,25);
		st_dialog=33;
	}
	else if(st_dialog==33){
		Dialog(4,11,0,0,26);
		st_dialog=34;
	}
	else if(st_dialog==34){
		Dialog(4,11,0,0,27);
		st_dialog=35;
	}
	else if(st_dialog==35){
		Dialog(4,6,0,0,28);
		st_dialog=36;
	}
	else if(st_dialog==36){
		Dialog(3,0,2,0,29);
		st_dialog=40;
	}
	else if(st_dialog==40){//轩辕剑闪光
		CDC *pdc=GetDC();
		MemDC.SelectObject(hbmp_15);//轩辕剑
		PlayWave("snd//move.wav");
		resetblt();
		blt(384,200,32,48,96,0,0,0,pdc);
		GameSleep(200);
		blt(384,190,32,48,128,0,32,0,pdc);
		GameSleep(200);
		blt(384,180,32,48,160,0,64,0,pdc);
		GameSleep(200);
		blt(384,170,32,48,128,0,32,0,pdc);
		GameSleep(200);
		blt(384,160,32,48,96,0,0,0,pdc);
		GameSleep(200);
		blt(384,150,32,48,96,0,0,0,pdc);
		GameSleep(200);
		blt(384,140,32,48,128,0,32,0,pdc);
		GameSleep(200);
		blt(384,130,32,48,160,0,64,0,pdc);
		GameSleep(200);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,30);
		st_dialog=41;
	}
	else if(st_dialog==41){
		Dialog(2,0,0,0,31);
		st_dialog=42;
	}
	else if(st_dialog==42){
		Dialog(4,0,3,0,32);
		st_dialog=43;
	}
	else if(st_dialog==43){
		Dialog(4,14,0,0,33);
		st_dialog=44;
	}
	else if(st_dialog==44){
		Dialog(4,8,0,0,34);
		st_dialog=45;
	}
	else if(st_dialog==45){
		Dialog(3,6,0,0,35);
		st_dialog=46;
	}
	else if(st_dialog==46){
		Dialog(2,0,0,0,36);
		st_dialog=47;
	}
	else if(st_dialog==47){
		Dialog(4,8,0,0,37);
		st_dialog=48;
	}
	else if(st_dialog==48){
		Dialog(2,0,0,0,38);
		st_dialog=49;
	}
	else if(st_dialog==49){
		Dialog(4,8,0,0,39);
		st_dialog=50;
	}
	else if(st_dialog==50){//轩辕剑消失
		oldx=32;oldy=48;oldlx=384;oldly=130;
		PlayWave("snd//move.wav");
		CDC *pdc=GetDC();
		blt(0,0,0,0,0,0,0,0,pdc);
		ReleaseDC(pdc);
		pdc = NULL;
		GameSleep(500);
		st_dialog=51;
		Dialog(5,0,5,0,40);//参数5表示存盘
	}
	else if(st_dialog==51){
		vclose();
		MemDC.SelectObject(hbmp_16);//火山的图片
		BuffDC.BitBlt(0,0,800,480,&MemDC,0,0,BLACKNESS);
		BuffDC.BitBlt(200,0,400,480,&MemDC,0,0,SRCCOPY);
		vopen();
		Dialog(2,0,0,0,41);
		st_dialog=60;
	}
	else if(st_dialog==60){//送别的场景
		vclose();
		//PlayMidi("midi\\p1004.mid");
		PlayMidi("midi\\p1013.mid");
		lclose();
		OpenMap("map\\maps101.bmp","map\\map101a.map","map\\map101b.npc");//调入地图
		current.x=28;
		current.y=120;
		DrawMap();
		BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_0);//卡诺
		resetblt();
		blt(384,208,32,48,96,0,0,0,&BuffDC);
		MemDC.SelectObject(hbmp_2);//老头
		resetblt();
		blt(352,208,32,48,96,0,0,0,&BuffDC);
		MemDC.SelectObject(hbmp_1);//荻娜
		resetblt();
		blt(416,208,32,48,96,0,0,0,&BuffDC);
		MemDC.SelectObject(hbmp_3);//司徒
		resetblt();
		blt(512,300,32,32,64,128,0,128,&BuffDC);
		MemDC.SelectObject(hbmp_8);//白莲教群体
		resetblt();
		blt(320,300,96,32,192,64,0,64,&BuffDC);
		MemDC.SelectObject(hbmp_7);//莫隆
		resetblt();
		blt(416,300,32,32,64,64,0,64,&BuffDC);
		vopen();
		Dialog(4,0,1,0,42);
		st_dialog=61;
	}
	else if(st_dialog==61){
		Dialog(4,9,0,0,43);
		st_dialog=62;
	}
	else if(st_dialog==62){
		Dialog(4,0,32,0,44);
		st_dialog=63;
	}
	else if(st_dialog==63){
		Dialog(4,11,0,0,45);
		st_dialog=64;
	}
	else if(st_dialog==64){
		Dialog(4,0,16,0,46);
		st_dialog=65;
	}
	else if(st_dialog==65){
		Dialog(4,0,5,0,47);
		st_dialog=66;
	}
	else if(st_dialog==66){
		Dialog(4,11,0,0,48);
		st_dialog=67;
	}
	else if(st_dialog==67){
		Dialog(4,26,32,0,49);
		st_dialog=70;
	}
	else if(st_dialog==70){//结束阶段
		GameSleep(200);
		vclose();
		//PlayMidi("midi\\p1013.mid");
		MemDC.SelectObject(&bmp_end);
		BuffDC.BitBlt(0,0,800,480,&MemDC,0,0,SRCCOPY);
		MemDC.SelectObject(hbmp_3);//司徒
		resetblt();
		blt(608,310,32,32,64,128,0,128,&BuffDC);
		MemDC.SelectObject(hbmp_8);//白莲教群体
		resetblt();
		blt(640,310,32,32,224,64,32,64,&BuffDC);
		MemDC.SelectObject(hbmp_7);//莫隆
		resetblt();
		blt(672,310,32,32,64,64,0,64,&BuffDC);
		MemDC.SelectObject(hbmp_17);//岛屿
		resetblt();
		blt(300,100,253,71,0,71,0,0,&BuffDC);
		vopen();
		GameSleep(3000);
		CDC *pdc=GetDC();
		pdc->BitBlt(300,100,253,71,&MemDC,0,71,SRCAND);
		MemDC.SelectObject(&bmp_end);
		for(i=0;i<150;i++){
			BuffDC.BitBlt((300+i),100,1,71,&MemDC,(300+i),100,SRCCOPY);
			pdc->BitBlt((300+i),100,1,71,&BuffDC,(300+i),100,SRCCOPY);
			BuffDC.BitBlt((553-i),100,1,71,&MemDC,(553-i),100,SRCCOPY);
			pdc->BitBlt((553-i),100,1,71,&BuffDC,(553-i),100,SRCCOPY);
			GameSleep(20);		
		}
		GameSleep(500);
		ReleaseDC(pdc);
		pdc = NULL;
		Dialog(2,0,0,0,55);
		st_dialog=71;
	}
	else if(st_dialog==71){
		Dialog(2,11,0,0,50);
		st_dialog=72;
	}
	else if(st_dialog==72){
		Dialog(2,6,0,0,51);
		st_dialog=73;
	}
	else if(st_dialog==73){
		Dialog(2,0,1,0,52);
		st_dialog=74;
	}
	else if(st_dialog==74){//请用户退出
		CDC *pdc=GetDC();
		MemDC.SelectObject(&bmp_end);
		BuffDC.BitBlt(0,0,800,480,&MemDC,0,0,SRCCOPY);
		pdc->BitBlt(0,0,800,480,&BuffDC,0,0,SRCCOPY);
		resetblt();
		for(i=0;i<150;i++){
			pdc->BitBlt(32,(320-i),550,160,&MemDC,32,(320-i),SRCCOPY);
			pdc->BitBlt(32,(320-i),550,160,&MemDC,0,480,SRCPAINT);
			GameSleep(50);
		}
		BuffDC.BitBlt(32,170,550,160,&MemDC,0,480,SRCAND);
		GameSleep(500);
		ReleaseDC(pdc);
		pdc = NULL;
		st_dialog=ST_DIALOG_NORMAL_EXIT;
		Dialog(10,0,0,0,54);
	}//游戏全部结束
}//st_sub1=1的部分结束
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para8Accident(int type)
{
	if(type==724){//游戏终结的位置
		StopActTimer();
		m_info_st=9;
		m_oldinfo_st=9;
		Dialog(4,0,36,0,9);
		st_sub1=1;
		st_dialog=0;
	}
}
//███████████████████████████████████████
//███████████████████████████████████████
void CMainFrame::Para8Timer()
{
	processenemies();
	BuffDC.BitBlt(50,45,84,22,&InfoDC,451,108,SRCAND);
	BuffDC.BitBlt(50,45,84,22,&InfoDC,368,108,SRCINVERT);
	BuffDC.StretchBlt(150,50,timelimit,9,&InfoDC,751,87,1,9,SRCCOPY);//时间
	if(timedelay>3){timelimit--;timedelay=0;}
	else{timedelay++;}//timedelay用于调整时间减少的速度
	if(timelimit==0){gameover();}
}
////////////////////////////////////////////////////////////////////////////////
void CMainFrame::Para8GameOver(CDC *pdc)
{
	vclose();
	StopActTimer();
	st_sub1=10;
	m_info_st=9;
	m_oldinfo_st=9;
	OpenMap("map\\maps110.bmp","map\\map110a.map","map\\map110a.npc");
	current.x=150;
	current.y=75;
	DrawMap();
	BuffDC.BitBlt(0,0,800,480,&MapDC,0,0,SRCCOPY);
	MemDC.SelectObject(hbmp_6);//恐怖分子
	resetblt();
	blt(128,192,32,32,64,128,64,32,&BuffDC);
	resetblt();
	blt(160,192,32,32,64,128,64,32,&BuffDC);
	resetblt();
	blt(128,224,32,32,64,128,64,32,&BuffDC);
	resetblt();
	blt(128,256,32,32,64,128,64,32,&BuffDC);
	resetblt();
	blt(160,256,32,32,64,128,64,32,&BuffDC);
	MemDC.SelectObject(hbmp_4);//阿萨德
	resetblt();
	blt(192,224,64,32,64,128,0,128,&BuffDC);
	MemDC.SelectObject(hbmp_1);//荻娜
	resetblt();
	blt(512,208,32,48,96,48,0,48,&BuffDC);
	vopen();
	GameSleep(500);
	MemDC.SelectObject(hbmp_4);//阿萨德开枪
	resetblt();
	oldx=64;oldy=32;oldlx=192;oldly=224;
	blt(192,224,64,32,64,160,0,160,pdc);
	PlayWave("snd\\shoot.wav");
	GameSleep(500);
	blt(192,224,64,32,64,128,0,128,pdc);
	MemDC.SelectObject(hbmp_1);//荻娜
	PlayWave("snd\\fdie.wav");
	oldx=32;oldy=48;oldlx=512;oldly=208;
	blt(512,208,32,48,128,192,32,192,pdc);
	GameSleep(2000);
}
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████
//███████████████████████████████████████